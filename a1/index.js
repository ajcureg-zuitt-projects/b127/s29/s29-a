/*
Activity:
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S29.
9. Initialize a local git repository, add the remote link and push to git with the commit message of s29 Activity.
10. Add the link in Boodle named "29 Introduction to Express js".
*/


const express = require('express');
const app = express()
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended:true }))



// 1
app.get('/home', (req, res)=> {
	res.send("Hi, Welcome to the homepage!")
})


// 3
let users = [
	{
		"name": "Eya",
		"username": "eyaaaa",
		"email": "eya1234@gmail.com"
	},
	{
		"name": "Klien",
		"username": "klienini",
		"email": "klien1234@gmail.com"
	},
	{
		"name": "Ryn",
		"username": "rynryn",
		"email": "ryn1234@gmail.com"
	},
];
app.get('/users', (req, res)=> {
	res.send(users)
})


// 5
app.delete('/delete-user', (req, res)=> {
	console.log(req.body)

	let message;
		for(let i = 0; i < users.length; i++){
        if(req.body.username == users[i].username){

            users[i].username = req.body.username
            users.splice(i,1)


            message = `User ${req.body.username} has been deleted`
            break;

        }else{
            message  = "user does not exist"
        }
    
    }
    res.send(message)
})



app.listen(port, () => console.log(`Server is running at port ${port}`))
